<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
	<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>${titulo}</title>
		
		<c:set var="path" value="${pageContext.request.contextPath}" scope="request" />
		<style type="text/css">
			@IMPORT url("${path}/static/bootstrap/css/bootstrap.min.css")		
		</style>
	</head>
	<body>
		<section class="container">
			<table class="table table-hover table-condensed table-striped table-bordered">
				<thead>
					<tr>
						<td>Código</td>
						<td>Nome</td>
						<td>Categoria</td>				
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${ingredientes}" var="ingrediente">
						<tr>
							<td>${ingrediente.id}</td>
							<td>${ingrediente.nome}</td>
							<td>${ingrediente.categoria}</td>
						</tr>
					</c:forEach>
				<tfoot>
					<tr>
						<td colspan="3">Quantidade de ingredientes: ${ingredientes.size()}</td>
					</tr>
				</tfoot>
				</tbody>
			</table>
		</section>
	</body>
</html>