package br.com.pizzaria.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.pizzaria.entidade.Pizza;
import br.com.pizzaria.service.PizzaService;

@Controller
@RequestMapping("/pizzas")
public class PizzaController{
	
	@Autowired private PizzaService pizzaService;
	
	@RequestMapping("/ola/{nome}")
	@ResponseBody
	public String msg(@PathVariable String nome) {
		return "Ol� pizza " + nome;
	}
	
	@RequestMapping("/consultaPizzas")
	@ResponseBody
	public String consultaQtdIngredientePizza() {
		Pizza pizza = pizzaService.criaPizza();
		return "Existem " + pizzaService.consultaQtdIngredientePizza(pizza) +" ingredientes cadastrados!";
	}
	

}
