package br.com.pizzaria.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.pizzaria.entidade.Pizza;
import br.com.pizzaria.service.PizzaService;

// requisi��o: /app/ingredientes sendo m�todo GET -> chama listarIngredientes
//requisi��o: /app/ingredientes sendo m�todo POST -> chama salvarIngredientes


@Controller
@RequestMapping("/ingredientes")
public class IngredienteController {
	
	@Autowired PizzaService pizzaService;
	
// retorno: procura a pagina em /WEB-INF/ingrediente/listagem.jsp 
	@RequestMapping(method=RequestMethod.GET)
	public String listarIngredientes(Model model) {
		model.addAttribute("titulo", "Listagem de Ingredientes");
		Pizza pizza = pizzaService.criaPizza();
		model.addAttribute("ingredientes", pizza.getListaIngrediente());
		return "ingrediente/listagem";
	}
	

}
