package br.com.pizzaria.entidade;

import br.com.pizzaria.enumaracoes.CategoriaIngredienteEnum;

public class Ingrediente {
	
	private Long id;
	
	private String nome;
	
	private CategoriaIngredienteEnum categoria;
	
	public Ingrediente(Long id, String nome, CategoriaIngredienteEnum categoria) {
		this.setId(id);
		this.setNome(nome);
		this.setCategoria(categoria);
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public CategoriaIngredienteEnum getCategoria() {
		return categoria;
	}
	public void setCategoria(CategoriaIngredienteEnum categoria) {
		this.categoria = categoria;
	}

}
