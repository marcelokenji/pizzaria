package br.com.pizzaria.entidade;

import java.util.List;

import br.com.pizzaria.enumaracoes.CategoriaPizzaEnum;

public class Pizza {
	
	private Long id;
	
	private String nome;
	
	private Double preco;
	
	private CategoriaPizzaEnum categoriaPizza;
	
	private List<Ingrediente> listaIngrediente;
	

	public Pizza(Long id, String nome, Double preco, CategoriaPizzaEnum categoria, List<Ingrediente> listaIngrediente) {
		this.setId(id);
		this.setNome(nome);
		this.setPreco(preco);
		this.setCategoriaPizza(categoria);
		this.setListaIngrediente(listaIngrediente);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Double getPreco() {
		return preco;
	}

	public void setPreco(Double preco) {
		this.preco = preco;
	}

	public CategoriaPizzaEnum getCategoriaPizza() {
		return categoriaPizza;
	}

	public void setCategoriaPizza(CategoriaPizzaEnum categoriaPizza) {
		this.categoriaPizza = categoriaPizza;
	}

	public List<Ingrediente> getListaIngrediente() {
		return listaIngrediente;
	}

	public void setListaIngrediente(List<Ingrediente> listaIngrediente) {
		this.listaIngrediente = listaIngrediente;
	}
	
	

}
