package br.com.pizzaria.service;

import java.util.ArrayList;

import br.com.pizzaria.entidade.Ingrediente;
import br.com.pizzaria.enumaracoes.CategoriaIngredienteEnum;

public class IngredienteService {
	
	private  ArrayList<Ingrediente> listaIngredientes;
	
	public ArrayList<Ingrediente> crialistaIngredientes() {
		Ingrediente ingrediente = new Ingrediente(1L, "calabresa", CategoriaIngredienteEnum.CARNE);
		Ingrediente ingrediente02 = new Ingrediente(2L, "queijo", CategoriaIngredienteEnum.CRIOS);
		
		listaIngredientes = new ArrayList<Ingrediente>();
		listaIngredientes.add(ingrediente);
		listaIngredientes.add(ingrediente02);
		return listaIngredientes;
	}


}
