package br.com.pizzaria.service;

import org.springframework.stereotype.Service;

import br.com.pizzaria.entidade.Pizza;
import br.com.pizzaria.enumaracoes.CategoriaPizzaEnum;

@Service
public class PizzaService {
	private Pizza pizza;

	private IngredienteService ingredieteService;
	
	public Pizza criaPizza(){
		ingredieteService = new IngredienteService();
		return pizza = new Pizza(1L, "portuguesa", 10.0, CategoriaPizzaEnum.GRANDE, ingredieteService.crialistaIngredientes());
	}
	
	public int consultaQtdIngredientePizza(Pizza pizza){
		return pizza.getListaIngrediente().size();
	}


}
